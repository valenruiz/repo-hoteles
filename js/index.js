document.addEventListener('DOMContentLoaded', function(event) {

  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover({
    trigger: 'focus'
  });

  $('.carousel').carousel({
    interval: 100
  });

  $('#contacto').on('show.bs.modal', function (e) {
    console.log('El modal funciona')
  });

  $('#contacto').on('shown.bs.modal', function (e) {
    console.log('El modal funciona shown')
    $('#contactobtn').removeClass('btn-outline-secondary')
    $('#contactobtn').addClass('btn-primary')
    $('#contactobtn').prop('disabled', true)
  });

  $('#contacto').on('hide.bs.modal', function (e) {
    console.log('El modal funciona hide')
  });

  $('#contacto').on('hidden.bs.modal', function (e) {
    console.log('El modal funciona hidden')
    $('#contactobtn').removeClass('btn-primary')
    $('#contactobtn').addClass('btn-outline-secondary')
    $('#contactobtn').prop('disabled', false)
  });

})